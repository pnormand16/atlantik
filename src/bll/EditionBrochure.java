
package bll;

import dao.Passerelle;
import entites.BateauVoyageur;
import java.util.LinkedList;
import java.util.List;
import utilitaires.PDF;

public class EditionBrochure {

     public static void editerBrochure(){
   
        List<BateauVoyageur>  lesBateaux= new LinkedList<BateauVoyageur>();
         PDF monPdf = new PDF ("BrochureBateauxVoyageur.pdf");
         
         lesBateaux = Passerelle.chargerLesBateauxVoyageurs();
         for (BateauVoyageur monBatVoy : lesBateaux){
             monPdf.chargerImage(monBatVoy.getImageBatVot());
             monPdf.ecrireTexte(monBatVoy.toString());
         }
         
     }
   
}
